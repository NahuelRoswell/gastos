var elementos = [];
var montoTotal = 0;
var elemento = 0;

$(document).ready(function () {
    select();

    $("#botonAgregar").click(agregarElemento);

    $("#cambiarMonto").click(cambiarMonto);

    $("#sumarMonto").click(sumarMonto);

    $("#listaValores").on('click', ".checkbox", function () {
        if ($(this).is(':checked'))
            $("#footer").show();
        else{
            ocultarBotonEliminar();
        }
    });

    $("#botonEliminar").click(function () {
        $("input:checkbox:checked").each(borrarElemento);
    })
});

function ocultarBotonEliminar() {
    $("#footer").hide();
}

function agregarElemento() {
    ingresoCorrecto = verificarTodoCompletado();

    if (ingresoCorrecto) {
        var producto = generarProducto();
        limpiarCamposProducto(); 
        restarValor(producto);
        elementos.push(producto);
        insertarDatos(producto.nombre, producto.precio);

        $("#listaValores").append(`
            <p>
                <label>
                    <input type="checkbox" class="checkbox" value="`+producto.precio +`"/>
                    <span>
                        ` + producto.nombre + `: 
                        $` + producto.precio + `
                    </span>
                </label>
            </p>
        `)

       
    } else
        alert("Se deben completar todos los campos");
}

function verificarTodoCompletado() {
    todoCompletado = true;
    nombre = $("#producto").val().length > 0;
    precio = $("#precio").val().length > 0;

    todoCompletado = todoCompletado && nombre && precio;

    return todoCompletado;
}

function generarProducto() {
    var nombre = $("#producto").val();
    var precio = $("#precio").val();

    return new Producto(nombre, precio);
}

function limpiarCamposProducto() {
    var nombre = $("#producto").val("");
    var precio = $("#precio").val("");
}

function restarValor(producto) {
    montoTotal = montoTotal - producto.precio;
    actualizarMonto(montoTotal);
}

function actualizarMonto(monto){
    update(montoTotal,"monto");
    escribirMonto(monto);
}

function cambiarMonto() {
    montoTotal = $("#montoTotal").val();
    escribirMonto(montoTotal);

    $("#listaValores").empty();

    borrarTabla();
    crearTabla();
    insertarDatos("monto", montoTotal);
}

function sumarMonto() {
    valorASumar = $("#agregarMonto").val();
    montoTotal = parseFloat(montoTotal) + parseFloat(valorASumar);

    $("#agregarMonto").val("");

    actualizarMonto(montoTotal);
}

function borrarElemento () {
    montoTotal = parseFloat($(this).val()) + parseFloat(montoTotal);

    update(montoTotal,"monto");
    escribirMonto(montoTotal);

    $(this).parent().remove();
    borrarProducto($(this).attr("id"))

    ocultarBotonEliminar();
}

function escribirMonto(texto) {
    $("#montoTotal").val("");
    $("#titulo").text("$ " + texto);
}

function mostrarLista(datos) {
    var largo = datos.rows.length;

    escribirMonto(datos.rows[0].precio);

    for (var i = 1; i < largo; i++) {
        $("#listaValores").append(`
        <p>
            <label>
                <input type="checkbox" class="checkbox" value="`+datos.rows[i].precio +`" id="`+datos.rows[i].rowid +`"/>
                <span>
                ` + datos.rows[i].nombre + `: 
                $` + datos.rows[i].precio + `
                </span>
            </label>
        </p>
        `)
    }
}

var db = openDatabase("primeradb", "1.0", "primer intento de base de datos", 5 * 1024 * 1024);

//creo una tabla de persona
// id real uniqute,
var t_persona = "CREATE TABLE persona (nombre text, precio text)"

//Creo la query de inserción
var t_personaInsert = "insert into persona (nombre,precio) values (?,?)";

//creo la query para consutlar los datos
var t_personaSelect = "select rowid, * from persona";

var t_personaDelete = "delete from persona where rowid=?";

//transaction: se encarga de generar las transacciones 
//entre la base de datos y el cliente
function crearTabla() {
    db.transaction(function (tx) {
        tx.executeSql(t_persona, [], function (tx, result) {
                console.log("la tabla se creo bien! ");
            },
            function (tx, error) {
                console.log("error crear tabla");
            });
    });
}

//Inserto datos
function insertarDatos(nombre, precio) {
    db.transaction(function (tx) {
        tx.executeSql(t_personaInsert, [nombre, precio], function (tx, result) {
                console.log("dato insertado");
                $("#listaValores p:last label input").attr("id",result.insertId);
            },
            function (tx, error) {
                console.log("hubo un error al insertar dato" + error.message);
            });
    })
}

//Actualizo datos
function update(precio,nombre) {
    db.transaction(function (tx) {
        tx.executeSql('UPDATE persona SET precio=? WHERE nombre=?', [precio, nombre], function (tx, result) {
                console.log("el update se hizo con éxito!");
            },
            function (tx, error) {

            });
    })  
}

//select
function select() {
    db.transaction(function (tx) {
        tx.executeSql(t_personaSelect, [], function (tx, result) {
                montoTotal = result.rows[0].precio;
                console.log(montoTotal);
                mostrarLista(result);
            },
            function (tx, error) {
                console.log("hubo un error select ? ->" + error.message);
            });
    });
}

//borrar tabla
function borrarTabla() {
    db.transaction(function (tx) {
        tx.executeSql("DROP TABLE persona", [], function (tx, result) {
            console.log("se borro la tabla");

        }, function (tx, error) {
            console.log("hubo un error al borrar tabla" + error.message);
        })
    })
}

function borrarProducto(id) {
    db.transaction(function (tx) {
        tx.executeSql(t_personaDelete, [id], function (tx, result) {
            console.log("se borro el producto");
        }, function (tx, error) {
            console.log("hubo un error al borrar producto" + error.message);
        })
    })
}